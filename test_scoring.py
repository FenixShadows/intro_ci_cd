import classifier


class TestScoring:

    # TODO : Définir un seuil minimal pour le score
    def test_score(self):
        assert 0.8 <= classifier.compute_score()

    # TODO : Tester le score avec un estimateur naïf
    def test_naive_score(self):
        assert classifier.random_score() < classifier.compute_score()
